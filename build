#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
ROOT=$SCRIPT_DIR

cd "$TABTREE_LIB"

case $1 in

all )

  SRC="$ROOT/source/all.tree"
  TARGET="$ROOT/target/solid.ttl"
;;

ontology )

  SRC="$ROOT/source/ontology.tree"
  TARGET="$ROOT/target/solid_ontology.ttl"
;;

"solidify_sp" )

  TARGET="$ROOT/target/solidify_sp"
  TRANSLATION_PATH="$ROOT/translation"
  SHAPES_TTL="$TRANSLATION_PATH/products.shacl.ttl"
  VALIDATOR_PATH="$HOME/app/shacl_validator"
  REF_TABTREE="$TARGET/all_solid.tree"

  echo ""
  echo "* Generate SHACL shapes"
  clj -M:compile-ttl --source "$TRANSLATION_PATH/products.shacl.tree" --target $SHAPES_TTL

  echo ""
  echo "TRANSLATE"
  echo "* translate to products.tree"
  clj -M:translate --source "$ROOT/source/facts/softwares.tree"  --dictionary "$TRANSLATION_PATH/solid_products_dict.tree" --target "$TARGET/products.tree"
  echo "* translate to people.tree"
  clj -M:translate --source "$ROOT/source/facts/people.tree"  --dictionary "$TRANSLATION_PATH/solid_products_dict.tree" --target "$TARGET/people.tree"
  echo "* translate all to ref tabtree"
  clj -M:translate --source "$ROOT/source/main.tree"  --dictionary "$TRANSLATION_PATH/solid_products_dict.tree" --target $REF_TABTREE

  echo ""
  echo "MAKE RDF"
  echo "* make products.ttl"
  clj -M:compile-ttl --source "$TARGET/products.ttl.tree" --target "$TARGET/products.ttl" --ref-tabtree $REF_TABTREE
  echo "* make people.ttl"
  clj -M:compile-ttl --source "$TARGET/people.ttl.tree" --target "$TARGET/people.ttl" --ref-tabtree $REF_TABTREE

  echo ""
  echo "VALIDATE SHACL SHAPES"
  echo "* Validate products.ttl"
  java -jar $VALIDATOR_PATH/validator.jar -contentToValidate "$TARGET/products.ttl" -externalShapes $SHAPES_TTL -reportSyntax "text/turtle"
  # java -jar validator.jar -contentToValidate $TARGET_TTL -externalShapes $SHAPES_TTL
  echo ""
  echo "* Validate people.ttl"
  java -jar $VALIDATOR_PATH/validator.jar -contentToValidate "$TARGET/people.ttl" -externalShapes $SHAPES_TTL -reportSyntax "text/turtle"

  exit 0
;;

"help" )
  echo "*** INFO ***"
  echo ""
  echo "This script builds RDF from Tabtree source files."
  echo ""
  echo "Command options"
  echo ""
  echo "'./build' - builds one big RDF."
  echo ""
  echo "'./build solidify_sp' - builds several RDF files, as required by 'Solidify Solid Project website' project. It follows its own subset of terms, so the script translates current terms and checks the correctness of conformation with a SHACL validator."
  echo ""
  echo "'./build help' - shows this info."
  echo ""
  echo "Please, check generated files in the 'target' dir"
  echo ""
  echo "*** END OF INFO ***"

  exit 0
;;

esac

echo ""
echo "* Check undeclared objects and predicates"
clj -M:all-undeclared --tabtree $SRC --ignore-keys ""

echo ""
echo "* Generate RDF graph (Turtle format) from Tabtree source files"
clj -M:compile-ttl --source $SRC --target $TARGET

echo ""
echo "* Validate Turtle code"
ttl $TARGET

echo ""
echo "* Some SPARQL..."
echo "** Graph statistics:"
arq --data $TARGET --query $ROOT/sparql/kgr_stat.rq

echo "** Ontology statistics:"
arq --data $TARGET --query $ROOT/sparql/ontology_counts.rq

echo "** Individuals statistics:"
arq --data $TARGET --query $ROOT/sparql/counts.rq

cd - 1>/dev/null
