#!/bin/bash

cd /home/denis/projects/clj_tabtree

clj -M:sort --by-id a-z --pars-order "name,nick,abbr,forum-id,bdate,ddate,bplace,place,dplace,sec-edu,edu,bachelor,master,job,creator,project,webid,git,url,personal-page-url,blog-url,essay-url,fb,twi,mastodon,tg,vk,yt,wg,panel,d" --ignore-keys "" --string-pars "name,d,forum-id" /home/denis/data/solid_kgr/source/facts/people.tree
