#!/bin/bash

cd /home/denis/projects/clj_tabtree

clj -M:sort --by-id a-z --pars-order "a,name,deabbr,part-of,from,based-on,plugin-for,driver-for,lang,webid,url,git,pub-doi,doc,start,end,company,project,initiative,author,developer,contributor,d,bug" --ignore-keys "" --string-pars "name,deabbr,pub-doi,d,bug" /home/denis/data/solid_kgr/source/facts/softwares.tree

cd -
