# Solid Knowledge Graph

A not as that small RDF knowledge graph that collects facts related to ecosystem around [SoLiD project](https://solidproject.org): personalities, apps, libraries, standards and websites.

For development I used home-baked Tabtree language intended to code knowledge in the modular way. [Clj-tabtree](https://codeberg.org/prozion/clj-tabtree) assembles tabtree files into a single RDF/Turtle file *(uberRDF)*.

This *uberRDF* is ready for the Solid pod and for SPARQL queries from the client app. You can use [my-solid-agent](https://codeberg.org/prozion/my-solid-agent) for this purpose.

## Setup

* First, to pull neccessary Clojure libraries, install [*My-solid-agent*](https://codeberg.org/prozion/my-solid-agent)
* Then add `export TABTREE_LIB=~/.gitlibs/_repos/https/codeberg.org/prozion/clj-tabtree` to your `~/.bashrc`
* `$ source ~/.bashrc`
* Turtle validator
  * Install Node.js (version 12+):
    * Add repositories: `curl -sL https://deb.nodesource.com/setup_20.x | sudo -E bash -`
    * `sudo apt install nodejs`
  * Install npm package manager: `sudo apt install npm`
  * Install validator: `sudo npm install -g turtle-validator`
  * Make runnable link: `sudo ln -s /usr/local/lib/node_modules/turtle-validator/TurtleValidator.js /usr/local/bin/ttl
* SHACL validator
  * Download [zip archive](https://www.itb.ec.europa.eu/shacl-offline/any/validator.zip)

For better experience with editing Tabtree format files, install [Tabtree syntax plugin](https://github.com/prozion/tabtree-atom-syntax) in [Pulsar code editor](https://pulsar-edit.dev/).

## Usage

### Step 1
Add or edit data in `*.tabtree` files

![Tabtree file with syntax highlightning](docs/images/tabtree1.png)

### Step 2
Build RDF from tabtrees: `$ ./build.sh`

![Build command screenshot](docs/images/build1.png)

### Step 3
Place resulting RDF on your Solid pod, so it will be accessible by others.

Personally, I use [PodPro IDE](https://podpro.dev/) to copy-paste Turtle code from local file to POD resource. Just log-in with your Solid Identity Provider and open your POD just like a project in a general IDE.

## Request RDF

As a small example, let's find contributors to Solid software via SPARQL query. Wherever possible, detect what country they are from.

In Solid knowledge graph, person would usually have various stated relations to software, but not as contributor directly: as developer, as creator, author etc. But we can use general property `:has_contributor` and get them all. The trick that seems harder to do in SQL.

Also in the graph, for person it is usually stated `:place`, but not `:country`. But country can be inferred: in `OPTIONAL` block we use the chaining with intermediate node (which is `?place` in this case)


```sparql
# software-contributors.rq

prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix afn: <http://jena.apache.org/ARQ/function#>
prefix : <https://purl.org/prozion/solid#>

SELECT DISTINCT ?person
                (group_concat(distinct(afn:localname(?software)); separator=", ") as ?contributed_to)
                (sample(?country) as ?from_country)
WHERE {
  ?person a :Person .
  ?software ?has_contributor ?person .
  ?software a ?software_class .
  ?software_class rdfs:subClassOf* :Software .
  ?has_contributor rdfs:subPropertyOf* :contributor .
  OPTIONAL {
    ?person :place ?place .
    ?country :contains ?place .
  }
}
GROUP BY ?person
ORDER BY desc(count(distinct(?software)))
```

### Quering locally with Jena

Use `arq` command-line utility from Apache Jena suite, to query RDF:

![SPARQL request screenshot](docs/images/sparql1-result.png)

The location of *uberRDF* file can be either local or somewhere on the web.

Let's query POD, where the RDF had been put on the previous steps (my POD as an example):
`arq --data https://prozion.inrupt.net/public/data/solid_knowledge_graph.ttl --query sparql/software-contributors.rq`

Of course, the URI of RDF is not limited by only your POD. It can reside on someone else's POD.

### Quering with Comunica

If you didn't install Apache Jena, you can use online SPARQL query tool [Comunica](https://query.linkeddatafragments.org/).

In the field `Choose datasource` write the URI of Turtle file, and paste query into the field below. Then click `Execute query`.
