@prefix : <https://purl.org/prozion/solid#> . 
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .


:
  a :KnowledgeGraph ;
  rdfs:label "Knowledge graph on ecosystem around Solid platform of distributed web" .

:_
  a owl:Restriction, owl:Class ;
  owl:minCardinality 1 ;
  owl:onProperty :standard-url ;
  rdfs:subClassOf :Standard .

:abbr
  a rdf:Property ;
  :d "Abbreviation"^^xsd:string ;
  rdfs:range rdfs:Literal .

:AdjacentProject
  a owl:Class ;
  rdfs:subClassOf :Project .

:admin
  a rdf:Property ;
  rdfs:subPropertyOf :contributor .

:Administrator
  a owl:Class ;
  :d "Administrators are responsible for administration of tools, systems, and services used for advancing Solid" ;
  rdfs:subClassOf :ProjectFacilitator .

:Agent
  a owl:Class ;
  :def "An entity that can be considered as an acting subject" ;
  rdfs:subClassOf :Application, owl:Thing .

:API
  a owl:Class ;
  :deabbr "Application Programming Interface" ;
  rdfs:subClassOf :Product .

:AppExtension
  a owl:Class ;
  rdfs:subClassOf :Application .

:Application
  a owl:Class ;
  rdfs:subClassOf :Software .

:ApplicationLayerProtocol
  a owl:Class ;
  rdfs:subClassOf :InternetProtocol .

:Article
  a owl:Class ;
  rdfs:subClassOf :Text .

:artifact
  a rdf:Property ;
  rdfs:domain :Project ;
  rdfs:range :Product .

:author
  a rdf:Property ;
  rdfs:subPropertyOf :contributor .

:bachelor
  a rdf:Property ;
  :d "In what university the person received the bachelor degree." ;
  rdfs:range :University ;
  rdfs:subPropertyOf :edu .

:based-on
  a rdf:Property ;
  rdfs:subPropertyOf :uses .

:bdate
  a rdf:Property ;
  :d "Date of birth" ;
  rdfs:range :Date ;
  rdfs:subPropertyOf :date .

:blog-url
  a rdf:Property ;
  rdfs:subPropertyOf :website .

:Book
  a owl:Class ;
  rdfs:subClassOf :Text .

:Boolean
  a owl:Class ;
  rdfs:subClassOf rdfs:Literal .

:bplace
  a rdf:Property ;
  rdfs:range :Place ;
  rdfs:subPropertyOf :place .

:Browser
  a owl:Class ;
  rdfs:subClassOf :Application .

:BrowserExtension
  a owl:Class ;
  rdfs:subClassOf :AppExtension .

:BrowserInBrowser
  a owl:Class ;
  rdfs:subClassOf :ServerApplication .

:bug
  a rdf:Property ;
  :d "Describes bugs which occured while trying the software" ;
  rdfs:subPropertyOf :note .

:business-url
  a rdf:Property ;
  rdfs:subPropertyOf :website .

:byear
  a rdf:Property ;
  rdfs:range :Year ;
  rdfs:subPropertyOf :date .

:ByteCode
  a owl:Class ;
  rdfs:subClassOf :DataFormat .

:Camp
  a owl:Class ;
  rdfs:subClassOf :Event .

:campus
  a rdf:Property ;
  rdfs:domain :University ;
  rdfs:range :Place .

:ceo
  a rdf:Property ;
  rdfs:subPropertyOf :director .

:chair
  a rdf:Property ;
  rdfs:subPropertyOf :lead .

:Charity
  a owl:Class ;
  rdfs:subClassOf :Organization .

:chat
  a rdf:Property ;
  rdfs:subPropertyOf :url .

:ChatApplication
  a owl:Class ;
  rdfs:subClassOf :SocialApplication .

:ChromeExtension
  a owl:Class ;
  rdfs:subClassOf :BrowserExtension .

:city
  a rdf:Property ;
  rdfs:range :City ;
  rdfs:subPropertyOf :place .

:City
  a owl:Class ;
  rdfs:subClassOf :Place .

:ClientApp
  a owl:Class ;
  rdfs:subClassOf :Application .

:ClientBrowser
  a owl:Class ;
  rdfs:subClassOf :Browser, :ClientApp .

:CMS
  a owl:Class ;
  :deabbr "Content Management System" ;
  rdfs:subClassOf :WebPlatform .

:co-author
  a rdf:Property ;
  rdfs:subPropertyOf :contributor .

:co-founder
  a rdf:Property ;
  rdfs:domain :Person ;
  rdfs:range :Organization .

:codeberg
  a rdf:Property ;
  rdfs:subPropertyOf :git .

:CodeEditor
  a owl:Class ;
  rdfs:subClassOf :DeveloperTool .

:CodeRepositoryPlatform
  a owl:Class ;
  rdfs:subClassOf :DeveloperPlatform .

:Collection
  a owl:Class ;
  rdfs:subClassOf :Product .

:CommunicationProtocol
  a owl:Class ;
  rdfs:subClassOf :Product .

:communicator
  a rdf:Property ;
  rdfs:subPropertyOf :contributor .

:Community
  a owl:Class ;
  rdfs:subClassOf owl:Thing, :GroupOfPeople .

:company
  a rdf:Property .

:Company
  a owl:Class ;
  rdfs:subClassOf :Organization .

:compiles-to
  a rdf:Property ;
  rdfs:range :DataFormat ;
  rdfs:subPropertyOf :transpiles-to .

:conference
  a rdf:Property ;
  rdfs:range :Conference ;
  rdfs:subPropertyOf :event .

:Conference
  a owl:Class ;
  rdfs:subClassOf :Event .

:contains
  a rdf:Property ;
  rdfs:domain :Object ;
  rdfs:range :Object .

:contributor
  a rdf:Property ;
  rdfs:range :Product .

:ContributorTeam
  a owl:Class ;
  rdfs:subClassOf :WorkingGroup, :SolidProjectGroup .

:Country
  a owl:Class ;
  rdfs:subClassOf :Object .

:country
  a rdf:Property ;
  rdfs:range :Country .

:Crawler
  a owl:Class ;
  rdfs:subClassOf :Agent .

:Creator
  a owl:Class ;
  :d "Creators are responsible for maintaining and reviewing suggestions to solidproject.org which includes Solid content such as the newsletter This Month in Solid." ;
  rdfs:subClassOf :ProjectFacilitator .

:creator
  a rdf:Property ;
  rdfs:subPropertyOf :author .

:crowdfunding
  a rdf:Property ;
  rdfs:range :UrlCore .

:css-fw
  a rdf:Property ;
  rdfs:subPropertyOf :fw .

:d
  a owl:AnnotationProperty ;
  :d "Description of any entity" ;
  rdfs:range :String .

:data-model
  a rdf:Property ;
  rdfs:range :DataModel .

:DataFormat
  a owl:Class ;
  rdfs:subClassOf :Notation .

:DataModel
  a owl:Class ;
  rdfs:subClassOf :Product .

:Date
  a owl:Class ;
  rdfs:subClassOf :Time, rdfs:Literal .

:date
  a rdf:Property ;
  rdfs:range :Date .

:DateTime
  a owl:Class ;
  rdfs:subClassOf :Time .

:ddate
  a rdf:Property ;
  :d "Date of death" ;
  rdfs:range :Year ;
  rdfs:subPropertyOf :date .

:deabbr
  a rdf:Property ;
  :d "Deabbreviation"^^xsd:string ;
  rdfs:range rdfs:Literal .

:Decimal
  a owl:Class ;
  rdfs:subClassOf :Number .

:def
  a rdf:Property ;
  :d "Definition of the object or phenomenon" .

:DEIChair
  a owl:Class ;
  :d "DEI chairs are responsible for increasing Diversity, Equity, and Inclusion in the Solid Community." ;
  rdfs:subClassOf :ProjectFacilitator .

:DemoApp
  a owl:Class ;
  rdfs:subClassOf :Prototype, :Application .

:developer
  a rdf:Property ;
  rdfs:subPropertyOf :contributor .

:DeveloperPlatform
  a owl:Class ;
  rdfs:subClassOf :Platform .

:DeveloperTool
  a owl:Class ;
  rdfs:subClassOf :Application .

:dialect-of
  a rdf:Property ;
  rdfs:domain :Language ;
  rdfs:range :Language .

:director
  a rdf:Property ;
  rdfs:subPropertyOf :lead .

:DiscussionPlatform
  a owl:Class ;
  rdfs:subClassOf :ChatApplication .

:doc
  a rdf:Property ;
  rdfs:subPropertyOf :url .

:dplace
  a rdf:Property ;
  rdfs:range :Place ;
  rdfs:subPropertyOf :place .

:driver-for
  a rdf:Property ;
  rdfs:subPropertyOf :extension-for .

:Editor
  a owl:Class ;
  :d "Editors are responsible for maintaining and reviewing suggestions to the Solid specification." ;
  rdfs:subClassOf :ProjectFacilitator .

:editor
  a rdf:Property ;
  rdfs:subPropertyOf :contributor .

:edu
  a rdf:Property ;
  :d "Education, in what university (faculty) the person made the studies and what (s)he graduated" .

:Email
  a owl:Class ;
  rdfs:subClassOf :Uri .

:email
  a rdf:Property ;
  rdfs:range :Email .

:end
  a rdf:Property ;
  rdfs:range :Time .

:error-message
  a rdf:Property ;
  rdfs:subPropertyOf :bug .

:Event
  a owl:Class ;
  rdfs:subClassOf owl:Thing .

:event
  a rdf:Property ;
  rdfs:range :Event .

:EventDocument
  a owl:Class ;
  rdfs:subClassOf owl:Thing .

:ExperimentalSolidWebService
  a owl:Class ;
  rdfs:subClassOf :SolidWebService .

:extension-for
  a rdf:Property ;
  rdfs:domain :Software ;
  rdfs:range :Software .

:fb
  a rdf:Property ;
  :d "Facebook profile of the person or company" ;
  rdfs:subPropertyOf :social-network .

:FileManager
  a owl:Class ;
  rdfs:subClassOf :Application .

:Float
  a owl:Class ;
  rdfs:subClassOf :Number .

:former-job
  a rdf:Property ;
  rdfs:subPropertyOf :former-member .

:former-member
  a rdf:Property ;
  rdfs:range :Organization .

:former-name
  a rdf:Property ;
  rdfs:subPropertyOf :name .

:forum-id
  a rdf:Property ;
  :d "Person's nickname on the Solid Community Forum" ;
  rdfs:domain :Person ;
  rdfs:range :String .

:founder
  a rdf:Property ;
  rdfs:subPropertyOf :co-founder .

:Framework
  a owl:Class ;
  rdfs:subClassOf :Library .

:from
  a rdf:Property ;
  rdfs:domain owl:Thing ;
  rdfs:range owl:Thing .

:fw
  a rdf:Property ;
  :d :Framework ;
  rdfs:range :Framework ;
  rdfs:subPropertyOf :uses .

:GeolocationApp
  a owl:Class ;
  rdfs:subClassOf :Application .

:git
  a rdf:Property ;
  :d "A git repositories account of person or organization" ;
  rdfs:subPropertyOf :url .

:github
  a rdf:Property ;
  rdfs:subPropertyOf :git .

:gitlab
  a rdf:Property ;
  rdfs:subPropertyOf :git .

:GroupOfObjects
  a owl:Class ;
  rdfs:subClassOf owl:Thing .

:GroupOfPeople
  a owl:Class ;
  rdfs:subClassOf :GroupOfObjects .

:HackerCamp
  a owl:Class ;
  rdfs:subClassOf :Camp .

:has-member
  a rdf:Property .

:header
  a rdf:Property ;
  rdfs:domain :GroupOfPeople ;
  rdfs:range :Person .

:HelloWorldApp
  a owl:Class ;
  rdfs:subClassOf :Prototype .

:host
  a rdf:Property ;
  :d "Host of the event" ;
  owl:equivalentProperty :moderator ;
  rdfs:range :Person ;
  rdfs:subPropertyOf :organizer .

:hostess
  a rdf:Property ;
  rdfs:subPropertyOf :host .

:hosting
  a rdf:Property ;
  rdfs:range :HostingProvider .

:HostingProvider
  a owl:Class ;
  rdfs:subClassOf :WebService .

:hq
  a rdf:Property ;
  rdfs:domain :Organization ;
  rdfs:range :Place .

:Id
  a owl:Class ;
  rdfs:subClassOf rdfs:Literal .

:IDE
  a owl:Class ;
  :deabbr "Integrated Development Environment" ;
  rdfs:subClassOf :DeveloperPlatform, :CodeEditor .

:Idea
  a owl:Class ;
  rdfs:subClassOf owl:Thing .

:IdentityProvider
  a owl:Class ;
  rdfs:subClassOf :SolidWebService .

:implements-standard
  a rdf:Property ;
  rdfs:range :Standard .

:Initiative
  a owl:Class ;
  rdfs:subClassOf :WorkingGroup, :Idea, :GroupOfPeople .

:initiative
  a rdf:Property ;
  rdfs:range :Initiative .

:Integer
  a owl:Class ;
  rdfs:subClassOf :Number .

:interested-in
  a rdf:Property ;
  rdfs:range :String .

:InternetLayerProtocol
  a owl:Class ;
  rdfs:subClassOf :InternetProtocol .

:InternetProtocol
  a owl:Class ;
  rdfs:subClassOf :CommunicationProtocol .

:job
  a rdf:Property ;
  rdfs:domain :Person ;
  rdfs:range :Company ;
  rdfs:subPropertyOf :member .

:job-role
  a rdf:Property ;
  rdfs:range :JobRole .

:JobRole
  a owl:Class ;
  rdfs:subClassOf owl:Thing .

:KnowledgeGraph
  a owl:Class ;
  rdfs:subClassOf :DataModel .

:lab
  a rdf:Property ;
  rdfs:range :Laboratory ;
  rdfs:subPropertyOf :org .

:Laboratory
  a owl:Class ;
  rdfs:subClassOf :ResearchGroup .

:lang
  a rdf:Property ;
  rdfs:range :Language .

:Language
  a owl:Class ;
  rdfs:subClassOf :Product .

:lead
  a rdf:Property ;
  owl:inverseOf :leaded-by ;
  rdfs:subPropertyOf :job .

:leaded-by
  a rdf:Property ;
  rdfs:subPropertyOf :has-member .

:LearningProject
  a owl:Class ;
  rdfs:subClassOf :Prototype .

:Lecture
  a owl:Class ;
  rdfs:subClassOf :Event, :Presentation .

:LectureVideoRecord
  a owl:Class ;
  rdfs:subClassOf :Video .

:lib
  a rdf:Property ;
  rdfs:range :Library ;
  rdfs:subPropertyOf :uses .

:Library
  a owl:Class ;
  rdfs:subClassOf :DeveloperTool, :Software .

:LibraryCollection
  a owl:Class ;
  rdfs:subClassOf :Collection .

:linkedin
  a rdf:Property ;
  :d "Linkedin account of the person" ;
  rdfs:subPropertyOf :social-network .

:LinkLayerProtocol
  a owl:Class ;
  rdfs:subClassOf :InternetProtocol .

:LongRead
  a owl:Class ;
  rdfs:subClassOf :Post .

:MarkupLanguage
  a owl:Class ;
  rdfs:subClassOf :Notation, :Language .

:master
  a rdf:Property ;
  :d "In what university the person received the master degree." ;
  rdfs:range :University ;
  rdfs:subPropertyOf :edu .

:mastodon
  a rdf:Property ;
  rdfs:subPropertyOf :tweets, :user-id .

:matrix
  a rdf:Property ;
  rdfs:subPropertyOf :chat .

:MediaArtifact
  a owl:Class ;
  rdfs:subClassOf owl:Thing, :Product .

:meeting-notes
  a rdf:Property ;
  rdfs:domain :Event ;
  rdfs:range :UrlCore .

:MeetingProtocol
  a owl:Class ;
  rdfs:subClassOf :EventDocument .

:Meetup
  a owl:Class ;
  rdfs:subClassOf :Event .

:member
  a rdf:Property ;
  owl:inverse-of :has-member ;
  rdfs:range :Organization .

:merged-to
  a rdf:Property ;
  rdfs:domain :Organization ;
  rdfs:range :Organization ;
  rdfs:subPropertyOf :to .

:MicrobloggingPost
  a owl:Class ;
  rdfs:subClassOf :Post .

:mission
  a rdf:Property ;
  rdfs:domain :Organization ;
  rdfs:range :String .

:MobileApp
  a owl:Class ;
  rdfs:subClassOf :Application .

:moderator
  a rdf:Property .

:name
  a rdf:Property ;
  rdfs:domain :Object ;
  rdfs:range rdfs:Literal ;
  rdfs:subPropertyOf rdfs:label .

:Network
  a owl:Class ;
  rdfs:subClassOf :Organization .

:nickname
  a rdf:Property ;
  rdfs:range :String .

:NonProfitFoundation
  a owl:Class ;
  rdfs:subClassOf :NonProfitOrganization .

:NonProfitOrganization
  a owl:Class ;
  rdfs:subClassOf :Organization .

:notation
  a rdf:Property ;
  rdfs:domain :Language ;
  rdfs:range :Notation .

:Notation
  a owl:Class ;
  :note "Usually used interchangeably with *Syntax*, whereas *Syntax* has other meanings" ;
  rdfs:subClassOf :Product .

:note
  a rdf:Property ;
  :d "Analog to skos/note" ;
  rdfs:range rdfs:Literal .

:Number
  a owl:Class ;
  rdfs:subClassOf rdfs:Literal .

:Object
  a owl:Class ;
  rdfs:subClassOf owl:Thing .

:OfflineEvent
  a owl:Class ;
  rdfs:subClassOf :Event .

:old-personal-page-url
  a rdf:Property ;
  rdfs:subPropertyOf :personal-page-url, :old-url .

:old-url
  a rdf:Property ;
  rdfs:subPropertyOf :url .

:OnlineEvent
  a owl:Class ;
  rdfs:subClassOf :Event .

:OnlineMeetup
  a owl:Class ;
  rdfs:subClassOf :VideoCall .

:Ontology
  a owl:Class ;
  rdfs:subClassOf owl:Ontology, :Vocabulary .

:OntologyEditor
  a owl:Class ;
  rdfs:subClassOf :DeveloperTool .

:OntologyLanguage
  a owl:Class ;
  rdfs:subClassOf :Language .

:OperatingSystem
  a owl:Class ;
  rdfs:subClassOf :Platform .

:org
  a rdf:Property ;
  rdfs:range :Organization .

:Organization
  a owl:Class ;
  rdfs:subClassOf owl:Thing .

:organizer
  a rdf:Property .

owl:Thing
  a owl:Class .

:p-lang
  a rdf:Property ;
  rdfs:range :ProgrammingLanguage ;
  rdfs:subPropertyOf :lang .

:PackageManager
  a owl:Class ;
  rdfs:subClassOf :DeveloperTool .

:panel
  a rdf:Property ;
  rdfs:domain :Person ;
  rdfs:range :SolidPanel .

:parent
  a rdf:Property ;
  rdfs:domain :Organization ;
  rdfs:range :Organization .

:part-of
  a rdf:Property ;
  rdfs:domain :Object ;
  rdfs:range :Object .

:patreon
  a rdf:Property ;
  rdfs:subPropertyOf :crowdfunding .

:PeerReviewedArticle
  a owl:Class ;
  rdfs:subClassOf :Article .

:Person
  a owl:Class ;
  rdfs:subClassOf :Agent .

:personal-page-url
  a rdf:Property ;
  rdfs:domain :Person ;
  rdfs:subPropertyOf :website .

:phd
  a rdf:Property ;
  :d "In what university the person received PhD degree." ;
  rdfs:range :University ;
  rdfs:subPropertyOf :edu .

:pkg-manager
  a rdf:Property ;
  rdfs:range :PackageManager ;
  rdfs:subPropertyOf :uses .

:place
  a rdf:Property ;
  rdfs:range :Place .

:Place
  a owl:Class ;
  rdfs:subClassOf owl:Thing .

:Platform
  a owl:Class ;
  rdfs:subClassOf :Software .

:platform
  a rdf:Property ;
  rdfs:domain :WebService ;
  rdfs:range :Platform .

:plugin-for
  a rdf:Property ;
  rdfs:subPropertyOf :extension-for .

:pod
  a rdf:Property ;
  rdfs:subPropertyOf :url .

:Pod
  a owl:Class ;
  :deabbr "Point Of Data" ;
  rdfs:subClassOf owl:Thing .

:PodBrowser
  a owl:Class ;
  rdfs:subClassOf :SolidApp, :BrowserInBrowser, :Browser .

:Podcast
  a owl:Class ;
  rdfs:subClassOf :MediaArtifact .

:PodProvider
  a owl:Class ;
  rdfs:subClassOf :SolidWebService .

:PositiveInteger
  a owl:Class ;
  rdfs:subClassOf :Integer .

:Post
  a owl:Class ;
  rdfs:subClassOf :Text .

:post-nominal
  a rdf:Property ;
  rdfs:domain :University ;
  rdfs:range :String .

:Presentation
  a owl:Class ;
  rdfs:subClassOf owl:Thing .

:presenter
  a rdf:Property ;
  rdfs:subPropertyOf :communicator .

:Product
  a owl:Class ;
  rdfs:subClassOf :Object, owl:Thing .

:profession
  a rdf:Property ;
  rdfs:range :String .

:Programme
  a owl:Class ;
  rdfs:subClassOf :Initiative .

:ProgrammingLanguage
  a owl:Class ;
  rdfs:subClassOf :Notation, :Language .

:project
  a rdf:Property ;
  rdfs:range :UrlCore .

:Project
  a owl:Class ;
  rdfs:subClassOf :Initiative, owl:Thing .

:ProjectFacilitator
  a owl:Class ;
  rdfs:subClassOf owl:Thing, :Role .

:ProjectsCatalog
  a owl:Class ;
  rdfs:subClassOf :WebService .

:ProofOfConcept
  a owl:Class ;
  rdfs:subClassOf :Prototype .

:Property
  a owl:Class ;
  rdfs:subClassOf :Object .

:Prototype
  a owl:Class ;
  rdfs:subClassOf :Software .

:pub-doi
  a rdf:Property ;
  rdfs:range :String .

:rdf-url
  a rdf:Property ;
  rdfs:domain :Ontology ;
  rdfs:range :UrlCore .

:RDFFramework
  a owl:Class ;
  rdfs:subClassOf :Framework .

:RDFLibrary
  a owl:Class ;
  rdfs:subClassOf :Library .

rdfs:label
  a rdf:Property .

rdfs:Literal
  a owl:Class .

:research-group
  a rdf:Property ;
  rdfs:domain :Person ;
  rdfs:range :ResearchGroup .

:researcher
  a rdf:Property ;
  rdfs:domain :Person ;
  rdfs:range :ResearchGroup ;
  rdfs:subPropertyOf :member .

:ResearchGroup
  a owl:Class ;
  rdfs:subClassOf :WorkingGroup .

:ResearchGroupsNetwork
  a owl:Class ;
  rdfs:subClassOf :WorkingGroupsNetwork .

:ResearchInstitute
  a owl:Class ;
  rdfs:subClassOf :ResearchOrganization .

:ResearchOrganization
  a owl:Class ;
  rdfs:subClassOf :Organization .

:resource-format
  a rdf:Property .

:retired
  a rdf:Property ;
  rdfs:range :Boolean .

:rfc
  a rdf:Property ;
  rdfs:subPropertyOf :spec .

:Role
  a owl:Class ;
  :def "Specialization in the collective work" ;
  rdfs:subClassOf owl:Thing .

:Runtime
  a owl:Class ;
  rdfs:subClassOf :Platform .

:runtime
  a rdf:Property ;
  rdfs:range :Runtime .

:s
  a rdf:Property ;
  :d :Source ;
  rdfs:range :Source .

:s-url
  a rdf:Property ;
  rdfs:range :SourceUrl ;
  rdfs:subPropertyOf :url, :s .

:s-video-timing
  a rdf:Property ;
  rdfs:domain :SourcedStatement ;
  rdfs:range :Time .

:SerializationFormat
  a owl:Class ;
  rdfs:subClassOf :DataFormat .

:ServerApplication
  a owl:Class ;
  rdfs:subClassOf :Application .

:service-url
  a rdf:Property ;
  :d "URL by which the app can be accessed on the web" ;
  rdfs:subPropertyOf :url .

:slides
  a rdf:Property ;
  rdfs:domain :Presentation ;
  rdfs:range :UrlCore .

:social-network
  a rdf:Property ;
  rdfs:subPropertyOf :url .

:SocialApplication
  a owl:Class ;
  rdfs:subClassOf :WebApplication .

:SocialNetwork
  a owl:Class ;
  rdfs:subClassOf :WebService .

:Software
  a owl:Class ;
  rdfs:subClassOf :Product .

:solid-server
  a rdf:Property ;
  rdfs:domain :IdentityProvider ;
  rdfs:range :SolidServer .

:SolidApp
  a owl:Class ;
  rdfs:subClassOf :SolidSoftware .

:SolidCommunity
  a owl:Class ;
  rdfs:subClassOf :Community .

:SolidDemoApp
  a owl:Class ;
  rdfs:subClassOf :SolidApp, :DemoApp .

:SolidDeveloperTool
  a owl:Class ;
  rdfs:subClassOf :SolidSoftware .

:SolidFramework
  a owl:Class ;
  rdfs:subClassOf :SolidDeveloperTool, :Framework .

:SolidLibrary
  a owl:Class ;
  rdfs:subClassOf :SolidDeveloperTool, :Library .

:SolidOntology
  a owl:Class ;
  :d "An ontology that describes some of the Solid facets" ;
  rdfs:subClassOf owl:Thing, :Ontology .

:SolidPanel
  a owl:Class ;
  rdfs:subClassOf :SolidProjectGroup .

:SolidProjectGroup
  a owl:Class ;
  rdfs:subClassOf owl:Thing, :GroupOfPeople .

:SolidServer
  a owl:Class ;
  rdfs:subClassOf :WebServer, :SolidSoftware .

:SolidSoftware
  a owl:Class ;
  rdfs:subClassOf owl:Thing .

:SolidWebService
  a owl:Class ;
  rdfs:subClassOf owl:Thing, :WebService .

:Source
  a owl:Class ;
  rdfs:subClassOf owl:Thing .

:SourcedStatement
  a owl:Class ;
  rdfs:subClassOf rdf:Statement .

:sourcehut
  a rdf:Property ;
  rdfs:subPropertyOf :git .

:SourceUrl
  a owl:Class ;
  rdfs:subClassOf :UrlCore .

:SPARQLClient
  a owl:Class ;
  rdfs:subClassOf :Application .

:spec
  a rdf:Property ;
  rdfs:subPropertyOf :standard-url .

:spoker
  a rdf:Property ;
  :d "Person that makes report or has topical speach on the event." ;
  rdfs:range :Person .

:Standard
  a owl:Class ;
  owl:equivalentClass :_ ;
  rdfs:subClassOf :Product .

:standard-url
  a rdf:Property ;
  rdfs:subPropertyOf :doc .

:start
  a rdf:Property ;
  rdfs:range :Time .

:String
  a owl:Class ;
  rdfs:subClassOf rdfs:Literal .

:SyntaxExtension
  a owl:Class ;
  rdfs:subClassOf :Notation .

:SystemOperator
  a owl:Class ;
  rdfs:subClassOf :ProjectFacilitator .

:Taxonomy
  a owl:Class ;
  rdfs:subClassOf :Vocabulary .

:TestingFramework
  a owl:Class ;
  rdfs:subClassOf :TestSuite .

:TestSuite
  a owl:Class ;
  rdfs:subClassOf :DeveloperTool .

:Text
  a owl:Class ;
  rdfs:subClassOf :MediaArtifact .

:Time
  a owl:Class ;
  rdfs:subClassOf rdfs:Literal .

:title
  a rdf:Property ;
  rdfs:range rdfs:Literal .

:to
  a rdf:Property ;
  rdfs:domain owl:Thing ;
  owl:inverseOf :from ;
  rdfs:range owl:Thing .

:transpiles-to
  a rdf:Property ;
  rdfs:domain :Language ;
  rdfs:range :Notation .

:TransportLayerProtocol
  a owl:Class ;
  rdfs:subClassOf :InternetProtocol .

:Tweet
  a owl:Class ;
  rdfs:subClassOf :MicrobloggingPost .

:tweets
  a rdf:Property .

:twi
  a rdf:Property ;
  :d "Twitter, now X" ;
  rdfs:subPropertyOf :tweets, :user-id .

:uni
  a rdf:Property ;
  rdfs:range :University .

:University
  a owl:Class ;
  rdfs:subClassOf :ResearchOrganization .

:Uri
  a owl:Class ;
  rdfs:subClassOf rdfs:Literal, owl:Thing .

:url
  a rdf:Property ;
  rdfs:range :UrlCore .

:Url
  a owl:Class ;
  rdfs:subClassOf :Uri .

:UrlCore
  a owl:Class ;
  :d "Url with prefixes removed: http://, https://, https://www. etc. to make it neater" ;
  rdfs:subClassOf :Url .

:user-id
  a rdf:Property ;
  :d "Unique user's name in messenger, forum, social network etc." ;
  rdfs:range :String .

:uses
  a rdf:Property .

:uses-api
  a rdf:Property ;
  rdfs:range :API ;
  rdfs:subPropertyOf :uses .

:version-of
  a rdf:Property ;
  rdfs:domain :Software ;
  rdfs:range :Software .

:Video
  a owl:Class ;
  rdfs:subClassOf :MediaArtifact .

:video
  a rdf:Property ;
  rdfs:range :UrlCore .

:VideoCall
  a owl:Class ;
  rdfs:subClassOf :OnlineEvent .

:VideoHowto
  a owl:Class ;
  rdfs:subClassOf :Video .

:VideoLecture
  a owl:Class ;
  rdfs:subClassOf :Video .

:VideoStream
  a owl:Class ;
  rdfs:subClassOf :VideoCall .

:vimeo
  a rdf:Property ;
  rdfs:subPropertyOf :video .

:Vocabulary
  a owl:Class ;
  rdfs:subClassOf :Product .

:w3c
  a rdf:Property ;
  :d "Standard by some of W3C group" ;
  rdfs:subPropertyOf :spec .

:W3CWorkingGroup
  a owl:Class ;
  rdfs:subClassOf :WorkingGroup .

:WebApplication
  a owl:Class ;
  rdfs:subClassOf :Application .

:WebForum
  a owl:Class ;
  rdfs:subClassOf :WebService .

:WebID
  a owl:Class ;
  rdfs:subClassOf :UrlCore .

:webid
  a rdf:Property ;
  :d "Solid WebID" ;
  rdfs:range :WebID ;
  rdfs:subPropertyOf :url .

:webid-format
  a rdf:Property ;
  rdfs:domain rdf:Statement ;
  rdfs:range :Standard ;
  rdfs:subPropertyOf :resource-format .

:WebPlatform
  a owl:Class ;
  rdfs:subClassOf :WebApplication .

:WebServer
  a owl:Class ;
  rdfs:subClassOf :Application .

:WebService
  a owl:Class ;
  rdfs:subClassOf owl:Thing, :Project .

:website
  a rdf:Property ;
  rdfs:domain :Source ;
  rdfs:range :Website ;
  rdfs:subPropertyOf :url .

:Website
  a owl:Class ;
  rdfs:subClassOf :WebService .

:WebUtility
  a owl:Class ;
  rdfs:subClassOf :DeveloperTool .

:wg
  a rdf:Property ;
  rdfs:domain :Person ;
  rdfs:range :WorkingGroup ;
  rdfs:subPropertyOf :member .

:working-note
  a rdf:Property ;
  :d "Analog to skos/editorialNote" ;
  rdfs:subPropertyOf :note .

:WorkingGroup
  a owl:Class ;
  rdfs:subClassOf owl:Thing, :GroupOfPeople .

:WorkingGroupsNetwork
  a owl:Class ;
  rdfs:subClassOf :Network .

:year
  a rdf:Property ;
  rdfs:range :Year .

:Year
  a owl:Class ;
  rdfs:subClassOf :Time .

:yt
  a rdf:Property ;
  rdfs:subPropertyOf :video .

